<?php

ini_set('error_reporting', 'E_NONE');
set_time_limit(0);
ob_start();

//Domains servers to be checked
$vpb_domain_servers = array(
	'.id' 		=> array('whois.id','DOMAIN NOT FOUND'),
	'.co.id' 		=> array('whois.id','DOMAIN NOT FOUND'),
	'.web.id' 	=> array('whois.id','DOMAIN NOT FOUND'),
	'.ac.id' 		=> array('whois.id','DOMAIN NOT FOUND'),
	'.or.id' 		=> array('whois.id','DOMAIN NOT FOUND'),
	'.sch.id' 	=> array('whois.id','DOMAIN NOT FOUND'),
	'.my.id' 		=> array('whois.id','DOMAIN NOT FOUND'),
	'.biz.id' 	=> array('whois.id','DOMAIN NOT FOUND'),
	'.net.id' 	=> array('whois.id','DOMAIN NOT FOUND'),
	'.ponpes.id' 	=> array('whois.id','DOMAIN NOT FOUND'),
);

$vpb_domain_name_to_search = trim(strip_tags($_POST['domain']));

if(isset($_POST['domain']) && !empty($vpb_domain_name_to_search)) //Be sure that the domain name is set and field is not empty to proceed
{
	$vpb_cleaned_domain_name_to_search = str_replace(array('www.', 'http://'), NULL, $vpb_domain_name_to_search);

	if($vpb_cleaned_domain_name_to_search != "")
	{
		foreach($vpb_domain_servers as $vpb_domain_servers_ext => $vpb_domain_whois)
		{
			$vpb_availability = NULL;

			if($vpb_socket = fsockopen($vpb_domain_whois[0], 43))
			{
				fputs($vpb_socket, $vpb_cleaned_domain_name_to_search.$vpb_domain_servers_ext . "\r\n");
				while( !feof($vpb_socket) )
				{
					$vpb_availability .= fgets($vpb_socket,128);
				}
				fclose($vpb_socket);

				if(preg_match('/'.$vpb_domain_whois[1].'/', $vpb_availability))
				{
					echo '<div id="vasplus_pb"><div class="available"><div style="float:left; width:280px; border:0px solid; padding-top:3px; padding-bottom:2px;"><span>Available</span>' . $vpb_cleaned_domain_name_to_search. '<b>' . $vpb_domain_servers_ext .'</b> is Available</div><div style="float:right;" align="right"><div class="vpb_general_button" style="color:#FFF; padding:3px; font-family:Verdana, Geneva, sans-serif; font-size:11px; cursor:pointer; margin:0px;" onclick="vpb_buy_now(\''.$vpb_cleaned_domain_name_to_search.$vpb_domain_servers_ext.'\')">Beli</div></div><br clear="all" /></div></div><br />';
				}
				else
				{
					echo '<div id="vasplus_pb"><div class="taken"><span>not available</span>' . $vpb_cleaned_domain_name_to_search . '<b>' .$vpb_domain_servers_ext .'</b> Domain Sudah Digunakan</div></div><br />';
				}
			}
			else
			{
				echo '<br><div class="info">Maaf, ada kesalahan yang menghubungkan ke server. Pastikan bahwa Anda terhubung ke internet dan coba lagi. Terima kasih.</div>';
				return false;
			}
		}
	}
	else
	{
		echo '<div class="info">Masukkan nama domain pilihan Anda untuk mencari.</div>';
	}
}
else
{
	echo '<div class="info">Masukkan nama domain pilihan Anda untuk mencari.</div>';
}
?>
